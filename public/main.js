"use strict";

let bottomEdge;
let rightEdge;

let v = Math.max(window.innerHeight, window.innerWidth) / 500;
let vSpeed = -v;
let hSpeed = v;

logo.style.top = `${(window.innerHeight - logo.offsetHeight) / 2}px`;
logo.style.left = `${(window.innerWidth - logo.offsetWidth) / 2}px`;
logo.style.backgroundColor = '#f00';

const toInt = (prop) => Number(prop.slice(0, -2));

window.addEventListener('resize', (event) => {
    if (window.innerHeight < logo.offsetHeight || window.innerWidth < logo.offsetWidth) {
        // TODO
    }

    bottomEdge = window.innerHeight - logo.offsetHeight;
    rightEdge = window.innerWidth - logo.offsetWidth;

    let top = toInt(logo.style.top);
    let left = toInt(logo.style.left);
    if (top < 0) {
        logo.style.top = `${0}px`;
    } else if (top > bottomEdge) {
        logo.style.top = `${bottomEdge}px`;
    }
    if (left < 0) {
        logo.style.left = `${0}px`;
    } else if (left > rightEdge) {
        logo.style.left = `${rightEdge}px`;
    }
});

window.dispatchEvent(new Event('resize'));

(function main(time) {
    let top = toInt(logo.style.top) + vSpeed;
    let left = toInt(logo.style.left) + hSpeed;
    if (top <= 0 || top >= bottomEdge) {
        vSpeed *= -1;
        top += vSpeed;
        logo.style.backgroundColor = nextColor(logo.style.backgroundColor);
    }
    if (left <= 0 || left >= rightEdge) {
        hSpeed *= -1;
        left += hSpeed;
        logo.style.backgroundColor = nextColor(logo.style.backgroundColor);
    }
    logo.style.top = `${top}px`;
    logo.style.left = `${left}px`;
    window.requestAnimationFrame(main);
})();

// // //

const SIZE_TRIPLE_HEX = (0xff0000).toString(2).length;
const SIZE_SHIFT = (0xff).toString(2).length;

const circularShift = (n, k = SIZE_SHIFT) => (n >> k) | (n << (SIZE_TRIPLE_HEX - k)) & ((1 << SIZE_TRIPLE_HEX) - 1);

const rgbToInt = (rgb) => rgb.slice(4, -1).split(', ', 3).reduce((pv, cv, ci, a) => pv + (Number(cv) << (8 * (2 - ci))), 0);

const nextColor = (color) => `#${circularShift(rgbToInt(color)).toString(16).padStart(6, '0')}`;
